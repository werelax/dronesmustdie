$(function(){

	$("html").removeClass( 'no-js');


	//Custom forms
	$('.custom-radio, .custom-checkbox').custom_form();


	//Select Current Item
	function current_item(e) {
		//alert("pepe");
	  $(this).parent().find(".is-current").removeClass('is-current');
	  $(this).toggleClass('is-current');
	}
	$('.list-item').bind('click', current_item);




	//Overlay: Lightbox
	function show_overlay(e) {
		$("body").addClass('overlay-is-shown');
	}
	function hide_overlay(e) {
		$("body").removeClass('overlay-is-shown');
	}

	$('.battle-canvas-wrap').bind('click', show_overlay);
	$('.overlay .btn').bind('click', hide_overlay);
});

  