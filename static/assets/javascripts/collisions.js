"use strict";

// MOVEMENT

function vdistance(p1, p2) {
  var xdist = p2.x - p1.x,
      ydist = p2.y - p1.y;
  return sqrt(xdist*xdist + ydist*ydist);
}

function boundingBoxCollision(agent, box) {
  return ((agent.position.x - agent.radio) <= box.x ||
          (agent.position.y - agent.radio) <= box.y ||
          (agent.position.x + agent.radio) >= (box.x + box.width) ||
          (agent.position.y + agent.radio) >= (box.y + box.height));
}

function dynamicCollision(agent1, agent2) {

  function getAngle(v) {
    return Math.atan2(v.y, v.x);
  }

  function normalize(v) {
    var m = vdistance({ x: 0, y: 0 }, v);
    return { x: v.x/m, y: v.y/m };
  }

  function dotProduct(v1, v2) {
    return (v1.x * v2.x) + (v1.y * v2.y);
  }

  function collidingDistance(a1, a2) {
    return (a1.speed >= (vdistance(a1.position, a2.position) - (a1.radio + a2.radio)));
  }

  function makeStationary(a1, a2) {
    var v1 = { x: cos(a1.direction)*a1.speed, y: sin(a1.direction)*a1.speed },
        v2 = { x: cos(a2.direction)*a2.speed, y: sin(a2.direction)*a2.speed },
        v  = { x: v1.x - v2.x, y: v1.y - v2.y },
        a  = Object.create(a1);
    a.speed = vdistance({ x: 0, y: 0 }, v);
    a.direction = getAngle(v);
    return a;
  }

  var origA1 = agent1;
  agent1 = makeStationary(agent1, agent2);

  if (!collidingDistance(agent1, agent2)) { return false; }

  var n = { x: 1 * cos(agent1.direction), y: sin(agent1.direction) },
      c = { x: (agent2.position.x - agent1.position.x), y: (agent2.position.y - agent1.position.y) },
      d = dotProduct(c, n);
  // going towards
  if (d <= 0) { return false; }
  // not close enough
  var f = pow2(vdistance({x: 0, y:0}, c)) - pow2(d),
      r2 = pow2(agent1.radio + agent2.radio);
  if (f > r2) { return false; }
  // collision point
  var t = r2 - f,
      maxTravel = d - sqrt(t);
  // not fast enough for the collision
  if (maxTravel > agent1.speed) { return false; }
  // ok, the collide!! now... wat?
  var ratio = (agent1.speed === 0)? 0 : maxTravel / agent1.speed;
  agent1.destination.x = origA1.position.x + ((origA1.speed * ratio) * cos(origA1.direction));
  agent1.destination.y = origA1.position.y + ((origA1.speed * ratio) * sin(origA1.direction));
  return { a1: origA1, a2: agent2, d: maxTravel };
}
