"use strict";

(function() {
  var Commands = Base.Class.extend({
    initialize: function(options) {
      this.robot = options.robot;
    },
    getPosition: function() {
      return this.robot.position;
    },
    getLife: function() {
      return this.robot.life;
    },
    changeDirection: function(options) {
      this.robot.actionQueue.push({name: "changeDirection", options: options});
    },
    changeSpeed: function(options) {
      this.robot.actionQueue.push({name: "changeSpeed", options: options});
    },
    radar: function(options) {
      this.robot.actionQueue.push({name: "radar", options: _.extend(options, {callback: _.bind(this._radarResponse, this)})});
    },
    _radarResponse: function(radarResponse) {
      return radarResponse;
    },
    shoot: function(options) {
      this.robot.actionQueue.push({name: "shoot", options: options});
    }
  });


  var Agent = Base.Class.extend({
    constructor: function(options) {
      Agent["__super__"].constructor.apply(this, arguments);
      this.direction = options.direction;
    },
  })
  _.extend(Agent.prototype, Backbone.Events);

  var Shot = Agent.extend({
    initialize: function(options) {
      this.type = "Shot";
      this.radio = SHOTRADIO;
      this.speed = SHOTSPEED;
      this.position = Object.create(options.robot.position);
    },
    destroy: function() {
      this.trigger("shot:destroy", this);
    }
  });

  var Robot = Agent.extend({
    initialize: function(options) {
      this.type = "Robot";
      this.radio = ROBOTRADIO;
      this.life = ROBOTLIFE;
      this.name = options.name;
      this.color = options.color;
      this.team = options.team;
      this.speed = options.speed;
      this.actionQueue = options.actionQueue;
      this.commands = new Commands({robot: this});
      this.code = new Function("commands", options.code);
    },
    consumeAction: function() {
      return this.actionQueue.pop();
    },
    decreaseLife: function(damage) {
      this.life -= damage;
      if (this.life <= 0) {
        this.destroy();
      }
    },
    destroy: function() {
      this.trigger("robot:destroy", this);
    }
  });

  var World = Base.Class.extend({
    initialize: function(options) {
      this.size = WORLDSIZE;
      this.agents = [];
      //Possible agents: Robots, shots
      this.agents = _.union(this.agents, options.robots);
    }
  });

  var CollisionManager =  Base.Class.extend({
    initialize: function(options) {
      this.world = options.world;
    },
    detect: function(agent, world, queue) {
      var out = boundingBoxCollision(agent, { x: 0,
                                              y: 0,
                                              width: WORLDSIZE,
                                              height: WORLDSIZE });
      var collision = _.reduce(world.agents, function(acc, el) {
        if (el === agent) { return acc; }
        var col = dynamicCollision(agent, el);
        if (acc && col) {
          col = (acc.d < col.d)? acc : col;
        }
        return col || acc;
      }, false);

      if (collision) {
        // correct the projected movement
        agent.destination = collision.a1.destination;
        agent.speed = 0;
        // add the collision to the queue
        queue.push({
          agent1: agent,
          agent2: collision.a2,
          outsideWorld: out
        });
      } else if (out) {
        agent.destination = {
          x: coerce(agent.position.x +
                    (cos(agent.direction) * agent.speed),
                    agent.radio + 1,
                    WORLDSIZE - agent.radio - 1),
          y: coerce(agent.position.y +
                    (sin(agent.direction) * agent.speed),
                    agent.radio + 1,
                    WORLDSIZE - agent.radio - 1)
        };
        agent.speed = 0;
        queue.push({
          agent1: agent,
          agent2: {},
          outsideWorld: out
        });
      }
    }
  });

  var CollisionQueueDispatcher = Base.Class.extend({
    dispatch: function(options) {
      _.each(options.collisionQueue, function(collision) {
        var agent1 = collision.agent1,
            agent2 = collision.agent2 || null,
            outsideWorld = collision.outsideWorld;
        switch(true) {
        case this.isShotRobotCollision(agent1, agent2):
          agent1.decreaseLife(SHOTDAMAGE);
          agent2.destroy();
          break;
        case this.isRobotsCollision(agent1, agent2):
          console.log(agent1, agent1.life);
          agent1.decreaseLife(ROBOTCOLLISIONDAMAGE);
          break;
        case this.isShotWallCollision(agent1, outsideWorld):
          agent1.destroy();
          break;
        case this.isRobotWallCollision(agent1, outsideWorld):
          agent1.decreaseLife(WALLDAMAGE);
          break;
        case this.isShotsCollision(agent1, agent2):
          agent1.destroy();
          agent2.destroy();
          break;
        }
      }, this);
    },
    isShotRobotCollision: function(agent1, agent2) {
      return (agent1.type === "Robot" && agent2.type === "Shot");
    },
    isRobotsCollision: function(agent1, agent2) {
      return (agent1.type === "Robot" && agent2.type === "Robot");
    },
    isShotWallCollision: function(agent1, outsideWorld) {
      return (agent1.type === "Shot" && outsideWorld);
    },
    isRobotWallCollision: function(agent1, outsideWorld) {
      return (agent1.type === "Robot" && outsideWorld);
    },
    isShotsCollision: function(agent1, agent2) {
      return (agent1.type === "Shot" && agent2.type === "Shot");
    }
  });

  var MovementDispatcher = Base.Class.extend({
    initialize: function(options) {
      this.world = options.world;
      this.collisionManager = new CollisionManager({world: this.world});
      this.collisionQueueDispatcher = new CollisionQueueDispatcher();
    },
    projectMovement: function(agent) {
      agent.destination = {
        x: coerce(agent.position.x +
                  (cos(agent.direction) * agent.speed),
                  agent.radio,
                  WORLDSIZE - agent.radio),
        y: coerce(agent.position.y +
                  (sin(agent.direction) * agent.speed),
                  agent.radio,
                  WORLDSIZE - agent.radio)
      };
    },
    updatePosition: function(agent) {
      agent.position.x = agent.destination.x;
      agent.position.y = agent.destination.y;
    },
    executeMovements: function() {
      _.each(this.world.agents, this.projectMovement, this);
      _.each(this.world.agents, function(agent) {
        var queue = [];
        this.collisionManager.detect(agent, this.world, queue);
        this.updatePosition(agent);
        this.collisionQueueDispatcher.dispatch({collisionQueue: queue, world: this.world});
      }, this);
      _.each(this.world.agents, this.updatePosition, this);
      return this.world;
    }
  });

  var ActionDispatcher = Base.Class.extend({
    initialize: function(options) {
      this.world = options.world;
    },
    executeActions: function(options) {
      this.world = options.world;
      _.each(options.robots, _.bind(function(robot) {
        //Ejecuto el código del robot cliente que en teoría meterá una acción a la cola
        robot.code(robot.commands);
        //Consume una acción de la cola de acciones
        var action = robot.consumeAction();
        if (action) {
          this.executeAction(robot, action);
        }
      }, this));
      return this.world;
    },
    executeAction: function(robot, action) {
      switch(action.name) {
      case "changeDirection":
        this.changeDirection(robot, action.options);
        break;
      case "shoot":
        this.shoot(robot, action.options);
        break;
      case "changeSpeed":
        this.changeSpeed(robot, action.options);
        break;
      case "radar":
        var objectsDetected = this.radar(robot, action.options);
        action.options.callback(objectsDetected);
        break;
      }
      // console.log(robot.type + " " + robot.name);
      // console.log(action.name);
      // console.log(robot.position);
    },
    radar: function(robot, options) {
      var agentAngles = this._calculateAngles(robot),
          onScope = _.filter(agentAngles, function(robotAngle) {
            return (robotAngle.angle > options.angleFrom && robotAngle.angle < options.angleTo);
          });
      if (!_.isEmpty(onScope)) {
        return this._closest(robot, _.pluck(onScope, "agent"));
      } else {
        return {};
      }
    },
    _closest: function(robot, agents) {
      return _.min(agents, _.bind(function(agent){
        this._distance(robot, agent);
      }, this), _.first(agents));
    },
    _distance: function(agent1, agent2) {
      return Math.sqrt(Math.pow(agent1.position.x - agent2.position.x, 2) + Math.pow(agent1.position.y - agent2.position.y, 2));
    },
    _calculateAngles: function(robot) {
      return _.map(_.without(this.world.agents, robot), function(agent) {
        var deltaY = robot.position.y - agent.position.y,
            deltaX = robot.position.x - agent.position.y,
            angleInDegrees = Math.atan2(deltaY, deltaX) * 180 / Math.PI;
        return {angle: Math.abs(angleInDegrees), agent: agent};
      });
    },
    changeSpeed: function(robot, options) {
      robot.speed = options.speed;
    },
    shoot: function(robot, options) {
      if (this.ableToShoot()) {
        var shot = new Shot(_.extend({robot: robot},  options));
        this.listenTo(shot, "shot:destroy", this.destroyShot);
        this.world.agents.push(shot);
      }
    },
    destroyShot: function(shot) {
      this.trigger("shot:destroy", shot);
    },
    ableToShoot: function() {
      //TODO
      return true;
    },
    changeDirection: function(robot, options) {
      robot.speed = 1;
      robot.direction = options.direction;
    },
  });

  _.extend(ActionDispatcher.prototype, Backbone.Events);

  var Simulation = Base.Class.extend({
    initialize: function(options) {
      this.robots = this.initializeRobots(options.robots);
      this.world = this.initializeWorld();
      this.collisionManager = new CollisionManager({world: this.world});
      this.movementDispatcher = new MovementDispatcher({world: this.world});
      this.actionDispatcher = new ActionDispatcher({world: this.world});
      this.canvasManager = new CanvasManager({world: this.world});
      this.listenTo(this.actionDispatcher, "shot:destroy", this.destroyAgent);
      this.timer = setInterval(_.bind(function() {
        if (this.isGameFinished()) {
          this.finishGame();
        } else {
          this.tick();
        }
      }, this), 20);
    },
    initializeWorld: function() {
      return new World({robots: this.robots});
    },
    initializeRobots: function(robots) {
      return _.map(robots, _.bind(function(robot) {
        //Creación de los robots
        var mapRobot = new Robot(robot);
        this.listenTo(mapRobot, "robot:destroy", this.destroyAgent);
        //Posicionamiento de los robots en el mapa
        mapRobot.position = robot.position;
        // mapRobot.position = {x: this._getRandomInt(0, WORLDSIZE), y: this._getRandomInt(0, WORLDSIZE)};
        return mapRobot;
      }, this));
    },
    teamsAlive: function() {
      var robots = _.filter(this.world.agents, function(agent) {
        return (agent.type === "Robot");
      });
      return _.uniq(_.pluck(robots, "team"));
    },
    isGameFinished: function() {
      return (_.size(this.teamsAlive()) <= 1);
    },
    finishGame: function() {
      clearInterval(this.timer);
      var winner = _.first(this.teamsAlive());
      alert("Ha ganado " + winner);
    },
    destroyAgent: function(agent) {
      var index = _.indexOf(this.world.agents, agent);
      this.world.agents.splice(index, 1);
    },
    _getRandomInt: function(min, max) {
      return Math.floor(Math.random() * (max - min + 1)) + min;
    },
    tick: function() {
      //1. Movimientos automaticos
      this.movementDispatcher.executeMovements({world: this.world})
      //2. Colisiones (event driven)
      //3. Efectos de acciones (muertes, restar vida, etc)
      //4. Acciones
      this.actionDispatcher.executeActions({robots: this.robots, world: this.world});
      //5. Refrescar canvas
      this.canvasManager.refresh(this.world.agents);
    }
  });

  _.extend(Simulation.prototype, Backbone.Events);

  var CanvasManager = Base.Class.extend({
    initialize: function(options) {
      this.canvas = document.querySelector("#battlefield");
      this.ctx = this.canvas.getContext("2d");
      this.world = options.world;
      this.refresh(this.world.agents);
    },
    refresh: function(world) {
      clear(this.ctx);
      draw(this.ctx, world);
    },

  });

  var acciones1 = "commands.changeSpeed({speed: 1})";
  var acciones2 = "if (Math.random() > 0.99) commands.shoot({direction: 6});";

  var robotJ1 = {name: "Robot1", direction: 0, speed: 1, life: 4, color: "red", team: "Paco", actionQueue:[], code: acciones1, position: {x: 100, y: 250}};
  var robotJ2 = {name: "Robot2", direction: 1.5, speed: 0, life: 4, color: "red", team: "Pepe", actionQueue:[], code: acciones2, position: {x: 300, y: 250}};

  new Simulation({robots: [robotJ1, robotJ2]});
})();
