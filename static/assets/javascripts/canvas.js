"use strict";

// DRAWING

function drawCircle(ctx, x, y, radius, color) {
  ctx.fillStyle = rgb(color);
  ctx.beginPath();
  ctx.arc(x, y, radius, 0, Math.PI*2, true);
  ctx.fill();
}

function drawSegment(ctx, x, y, length, direction, color) {
  // ctx.strokeStyle = rgb({r: 0, g: 0, b: 0});
  ctx.strokeStyle = color;
  ctx.beginPath();
  ctx.moveTo(x, y);
  ctx.lineTo(x + cos(direction)*length,
             y - sin(direction)*length);
  ctx.stroke();
}

function drawAgent(ctx, agent) {
  var x = agent.position.x,
      y = WORLDSIZE - agent.position.y,
      r = agent.radio,
      d = agent.direction,
      cr = {r: 100, g: 100, b: 100 },
      cs = {r: 200, g: 0, b: 0 }
  drawCircle(ctx, x, y, r, (agent.type === "Robot")? cr : cs);
  drawSegment(ctx, x, y, r + agent.speed, d, "red");
}

function clear(ctx) {
  ctx.fillStyle = colors["white"];
  ctx.fillRect(0, 0, WORLDSIZE, WORLDSIZE);
}

function draw(ctx, world) {
  _.each(world, function(agent) {
    drawAgent(ctx, agent);
  });
}

// window.onload = function() {
//   var canvas = document.querySelector("#canvas"),
//       ctx = canvas.getContext("2d");
//   start(ctx);
// };
