var cos = Math.cos,
    sin = Math.sin,
    sqrt = Math.sqrt,
    pow2 = function(n) { return Math.pow(n, 2); };

var colors = {
  "white": "rgb(255,255,255)"
};

function rgb(obj) {
  return ["rgb(", obj.r, ",", obj.g, ",", obj.b, ")"]
    .join("");
}

function rand(l, n) {
  if (!n) { n = l; l = 0; }
  return l + Math.floor(Math.random() * n);
}

function coerce(v, min, max) {
  return Math.max(min, Math.min(max, v));
}

window.requestAnimFrame = (function(){
  return  window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame    ||
    function( callback ){
      window.setTimeout(callback, 1000 / 60);
    };
})();

// debug
window.requestAnimFrame = function(cb) {
  window.setTimeout(cb, 1000 / 10);
};
