/* globals window */
"use strict";

// UTILS + CONFIG

var WIDTH = 500,
    HEIGHT = 500;

var cos = Math.cos,
    sin = Math.sin,
    sqrt = Math.sqrt,
    pow2 = function(n) { return Math.pow(n, 2); };

var colors = {
  "white": "rgb(255,255,255)"
};

function rgb(obj) {
  return ["rgb(", obj.r, ",", obj.g, ",", obj.b, ")"]
    .join("");
}

function rand(l, n) {
  if (!n) { n = l; l = 0; }
  return l + Math.floor(Math.random() * n);
}

function coerce(v, min, max) {
  return Math.max(min, Math.min(max, v));
}

window.requestAnimFrame = (function(){
  return  window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame    ||
    function( callback ){
      window.setTimeout(callback, 1000 / 60);
    };
})();

// debug
window.requestAnimFrame = function(cb) {
  window.setTimeout(cb, 1000 / 10);
};

// STATE

var world = [];

function mkAgent(x, y, radius, direction, speed, color) {
  return {x: x, y: y, r: radius, d: direction, s: speed, c: color};
}

function randomAgent() {
  return mkAgent(rand(15, 500),
                 rand(15, 500),
                 rand(3, 50),
                 rand(2*Math.PI),
                 rand(1, 30),
                 { r: rand(0, 255),
                   g: rand(0, 255),
                   b: rand(0, 255) });
}

function populateWorld(nAgents) {
  for(var i=0; i<nAgents; i++, world.push(randomAgent()));
}

// MOVEMENT

function vdistance(p1, p2) {
  var xdist = p2.x - p1.x,
      ydist = p2.y - p1.y;
  return sqrt(xdist*xdist + ydist*ydist);
}

function boundingBoxCollision(agent, box) {
  return ((agent.x - agent.r) <= box.x ||
          (agent.y - agent.r) <= box.y ||
          (agent.x + agent.r) >= (box.x + box.width) ||
          (agent.y + agent.r) >= (box.y + box.height));
}

function dynamicCollision(agent1, agent2) {

  function getAngle(v) {
    return Math.atan2(v.y, v.x);
  }

  function normalize(v) {
    var m = vdistance({ x: 0, y: 0 }, v);
    return { x: v.x/m, y: v.y/m };
  }

  function dotProduct(v1, v2) {
    return (v1.x * v2.x) + (v1.y * v2.y);
  }

  function collidingDistance(a1, a2) {
    return (a1.s >= (vdistance(a1, a2) - (a1.r + a2.r)));
  }

  function makeStationary(a1, a2) {
    var v1 = { x: cos(a1.d)*a1.s, y: sin(a1.d)*a1.s },
        v2 = { x: cos(a2.d)*a2.s, y: sin(a2.d)*a2.s },
        v  = { x: v1.x - v2.x, y: v1.y - v2.y },
        a  = Object.create(a1);
    a.s = vdistance({ x: 0, y: 0 }, v);
    a.d = getAngle(v);
    return a;
  }

  var origA1 = agent1;
  agent1 = makeStationary(agent1, agent2);

  if (!collidingDistance(agent1, agent2)) { return false; }

  var n = { x: 1 * cos(agent1.d), y: sin(agent1.d) },
      c = { x: (agent2.x - agent1.x), y: (agent2.y - agent1.y) },
      d = dotProduct(c, n);
  // going towards
  if (d <= 0) { return false; }
  // not close enough
  var f = pow2(vdistance({x: 0, y:0}, c)) - pow2(d),
      r2 = pow2(agent1.r + agent2.r);
  if (f > r2) { return false; }
  // collision point
  var t = r2 - f,
      maxTravel = d - sqrt(t);
  // not fast enough for the collision
  if (maxTravel > agent1.s) { return false; }
  // ok, the collide!! now... wat?
  var ratio = (agent1.s == 0)? 0 : maxTravel / agent1.s;
  // agent2.x = (agent2.s * ratio) * cos(agent2.d);
  // agent2.y = (agent2.s * ratio) * sin(agent2.d);
  agent1.destination.x = origA1.x + ((origA1.s * ratio) * cos(origA1.d));
  agent1.destination.y = origA1.y + ((origA1.s * ratio) * sin(origA1.d));
  return { a1: origA1, a2: agent2, d: maxTravel };
}

function calculateDestionation(agent) {
  agent.destination = {
    x: coerce(agent.x + (Math.cos(agent.d) * agent.s), agent.r, WIDTH - agent.r),
    y: coerce(agent.y + (Math.sin(agent.d) * agent.s), agent.r, HEIGHT - agent.r)
  };
  return agent;
}

function doMoveAgent(agent) {
  agent.x = agent.destination.x;
  agent.y = agent.destination.y;
  return agent;
}

function reverseDirection(d) {
  var pi2 = Math.PI * 2;
  return (pi2 + (d - Math.PI/2)) % pi2;
}

function randomDirection() {
  return Math.random()*2*Math.PI;
}

function reverseIfOut(agent) {
  var box = { x: 0, y: 0, width: WIDTH, height: HEIGHT };
  if (boundingBoxCollision(agent, box)) {
    // agent.d = reverseDirection(agent.d);
    agent.d = randomDirection();
    agent = calculateDestionation(agent);
  }
  return agent;
}

function reverseIfCollision(world, agent) {
  var collided = world.reduce(function(acc, el) {
    if (el === agent) { return acc; }
    var col = dynamicCollision(agent, el);
    if (!acc && col) {
      return col;
    } else if (col && col.d < acc.d) {
      return col;
    } else {
      return acc;
    }
  }, false);
  if (collided) { collided.a1.d = reverseDirection(collided.a1.d); }
  return collided? collided.a1 : agent;
}

function move(world) {
  world = world.map(calculateDestionation);
  world = world.map(reverseIfOut);
  world = world.map(reverseIfCollision.bind({}, world));
  world = world.map(doMoveAgent);
  return world;
}

// DRAWING

function drawCircle(ctx, x, y, radius, color) {
  ctx.fillStyle = rgb(color);
  ctx.beginPath();
  ctx.arc(x, y, radius, 0, Math.PI*2, true);
  ctx.fill();
}

function drawSegment(ctx, x, y, length, direction, color) {
  // ctx.strokeStyle = rgb({r: 0, g: 0, b: 0});
  ctx.strokeStyle = color;
  ctx.beginPath();
  ctx.moveTo(x, y);
  ctx.lineTo(x + Math.cos(direction)*length,
             y - Math.sin(direction)*length);
  ctx.stroke();
}

function drawAgent(ctx, agent) {
  var x = agent.x,
      y = HEIGHT - agent.y;
  drawCircle(ctx, x, y, agent.r, agent.c);
  drawSegment(ctx, x, y, agent.r, agent.d, "black");
  drawSegment(ctx, x, y, agent.r + agent.s, agent.d, "red");
}

function clear(ctx) {
  ctx.fillStyle = colors["white"];
  ctx.fillRect(0, 0, WIDTH, HEIGHT);

}

function draw(ctx, world) {
  world.forEach(function(agent) {
    drawAgent(ctx, agent);
  });
}

function onFrame(ctx) {
  world = move(world);
  clear(ctx);
  draw(ctx, world);
}

// INIT

populateWorld(4);
// world.push(mkAgent(250, 250, 150, 0, 0, {r: 255, g: 0, b: 0}));
// world.push(mkAgent(60, 60, 50, 2*Math.PI/8, 50, {r: 0, g: 255, b: 0}));

function start(ctx) {
  window.requestAnimFrame(function frame() {
    onFrame(ctx);
    window.requestAnimFrame(frame);
  });
}


window.onload = function() {
  var canvas = document.querySelector("#canvas"),
      ctx = canvas.getContext("2d");
  start(ctx);
};

// function inBox(objBox, containerBox) {
//   if (objBox.x > containerBox.x) {
//     return containerBox;
//   } else if ((objBox.x + objBox.width) < (containerBox.x + containerBox.width)) {
//     return containerBox;
//   }
//   return false;
// }

// function overlapBox(objBox, targetBox) {
//   //...
// }

// var world = [{x:0, y:0, width:100, height:100}, ...]

// var target = world[0];

// var collision = world.slice(1).reduce(function(acc, agent) {
//   return acc || overlapBox(target, agent);
// }, false);

// var outGoing = world.reduce(function(acc, agent) {
//   if (!inBox(agent, worldMap)) {
//     acc.push(agent);
//   }
//   return acc;
// }, []);
