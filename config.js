var express = require("express");

var app = express();

app.configure(function() {
  app.set("port", process.env.PORT || 8080);
  // app.use(express.favicon());
  app.use(express.logger("short"));
  app.use(express.bodyParser());
  app.use(express.methodOverride());

  app.set("views", __dirname + "/views");
  app.set("view engine", "jade");

  app.use(express.cookieParser("your secret here"));
  app.use(express.cookieSession({secret: "jarl"}));

  app.use(app.router);
  app.use(express.static(__dirname + "/static"));
});

module.exports = app;
