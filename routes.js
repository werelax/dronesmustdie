function resources(app, name, controller) {
  if (controller.index) { app.get("/"+name, controller.index); }
  if (controller["new"]) { app.get("/"+name+"/new", controller["new"]); }
  if (controller.create) { app.post("/"+name, controller.create); }
  if (controller.show) { app.get("/"+name+"/:"+name+"id", controller.show); }
  if (controller.edit) { app.get("/"+name+"/:"+name+"id/edit", controller.edit); }
  if (controller.update) { app.put("/"+name+"/:"+name+"id", controller.update); }
  if (controller["delete"]) { app["delete"]("/"+name+"/:"+name+"id", controller["delete"]); }
  if (controller.param) { app.param(name + "id", controller.param); }
}

module.exports = function(app, auth, controllers) {

  app.get("/login", controllers.users.login);
  app.post("/session", auth.createSession({ redirect: "/drones" }));
  app.get("/logout", auth.destroySession, controllers.users["new"]);
  app.get("/register", controllers.users["new"]);
  app.post("/register", controllers.users.create);
  app.get("/drones/versus", controllers.drones.versus);

  app.get("/", function(req, res) {
    if (auth.isLogged(req)) {
      res.redirect("/drones");
    } else {
      return controllers.static.home(req, res);
    }
  });

  app.get("/home", controllers.static.home);

  auth.withSession(app, function(app) {
    app.get("/me", controllers.users.show);
    resources(app, "drones", controllers.drones);
  });

  app.listen(3000);
};
