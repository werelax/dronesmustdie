var models = require("./models"),
    ObjectID = require("mongodb").ObjectID;

/* Auth */
module.exports = function(auth) {
  auth.setStrategy({
    serializeUser: function(user) {
      return user._id;
    },
    deserializeUser: function(userId, cb) {
      models.users("findOne", { _id: new ObjectID(userId) })
        .then(function(user) {
          return user;
        })
        .then(cb)
        .done();
    },
    checkCredentials: function(username, pass, cb) {
      models.users("findOne", {"$or": [{email: username}, {username: username}]})
        .then(function(user) {
          cb(null, (user && user.password === pass) ? user : null);
        });
    }
  });
};
