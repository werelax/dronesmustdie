module.exports = function(app, auth, models, socketManager) {
  "use strict";

  var controllers = {};

  controllers.drones = {
    index: function(req, res) {
      res.render("code");
    },
    "new": function(req, res) {
    },
    create: function(req, res) {
      res.render("versus");
    },
    show: function(req, res) {
      res.render("battle-canvas");
    },
    edit: function(req, res) {
    },
    update: function(req, res) {
    },
    "delete": function(req, res) {
    },
    versus: function(req, res) {
      res.render("versus");
    }
  };

  controllers.users = {
    "new": function(req, res) {
      res.sendfile("views/register.html");
    },
    show: function(req, res) {
      res.render("me");
    },
    create: function(req, res) {
      models.users("insert", {
        email: req.body.email,
        username: req.body.username,
        password: req.body.password
      }).then(function(user) {
        auth.login(req, user[0]);
        res.redirect("/me");
      });
    },
    login: function(req, res) {
      res.sendfile("views/login.html");
    }
  };

  controllers.static = {
    home: function(req, res) {
      res.render("home");
    }
  };

  return controllers;
}
