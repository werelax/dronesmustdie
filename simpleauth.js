function extend() {
  "use strict";
  var args = [].slice.call(arguments);
  return args.reduce(function(acc, el) {
    for (var k in el) {
      if (el.hasOwnProperty(k)) { acc[k] = el[k]; }
    }
    return acc;
  });
}

var strategy = {
  serializeUser: function() {
    // (user)
  },
  deserializeUser: function() {
    // (userId, cb)
  },
  checkCredentials: function() {
    // (username, pass, done)
  },
  loginRoute: "/login"
};

exports.setStrategy = function(customStrategy) {
  "use strict";
  strategy = extend({}, strategy, customStrategy);
};

exports.isLogged = function(req) {
  "use strict";
  return req.session.user !== undefined;
};

exports.login = function(req, user) {
  "use strict";
  req.session.user = strategy.serializeUser(user);
};

exports.createSession = function(options) {
  "use strict";
  var config = {
    username: "username",
    password: "password",
    redirect: "/me",
    failRedirect: strategy.loginRoute
  };
  config = extend({}, config, options);
  return function(req, res) {
    var username = req.body[config.username],
        pass = req.body[config.password];
    strategy.checkCredentials(username, pass, function(err, user) {
      if (!err && user) {
        console.log("Usuario logueado:",username);
        exports.login(req, user);
        res.redirect(config.redirect);
      } else {
        console.log("Credenciales incorrectas");
        res.redirect(config.failRedirect);
      }
    });
  };
};

exports.requiresSession = function(req, res, next) {
  "use strict";
  if (req.session.user) {
    strategy.deserializeUser(req.session.user, function(user) {
      if (!user)  {
        console.log("El usuario no existe!");
        delete req.session.user;
        res.redirect(strategy.loginRoute);
      } else {
        req.user = user;
        next();
      }
    });
  } else {
    console.log("No existe la sesión...");
    res.redirect(strategy.loginRoute);
  }
};

exports.requiresToken = function(req, res, next) {
  "use strict";
  var token = req.param("token");
  if (token !== undefined) {
    strategy.deserializeUserToken(token, function(user) {
      if (!user)  {
        console.log("El usuario no existe!");
        res.send(401, {error: "No autorizado"});
      } else {
        req.user = user;
        next();
      }
    });
  } else {
    console.log("No existe token...");
    res.send(401, {error: "No autorizado"});
  }
};

exports.destroySession = function(req, res, next) {
  "use strict";
  delete req.session.user;
  next();
};

function withAuth(authMethod) {
  "use strict";
  return function(app, cb) {
    function createAuthRouting(verb) {
      return function() {
        var route = arguments[0],
            args = [].slice.call(arguments, 1);
        return app[verb].apply(app, [route, authMethod].concat(args));
      };
    }
    var routeVerbs = Object.create(app);
    ["get", "post", "put", "delete"].forEach(function(v) {
      routeVerbs[v] = createAuthRouting(v);
    });
    cb(routeVerbs);
  };
}

exports.withSession = withAuth(exports.requiresSession);
exports.withToken = withAuth(exports.requiresToken);
