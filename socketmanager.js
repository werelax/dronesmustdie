"use strict";

var io = require("socket.io"),
    models = require("./models"),
    _ = require("underscore"),
    Q = require("q"),
    Simulation = require("./simulation");

io.of("/arena")
  .on("connection", function(socket) {
    var arena = {};
    socket.on("setup", function(setupData) {
      models.drones.findArray({"$or": [{token: setupData.player},
                                       {token: setupData.rival}]})
        .then(function(drones) {
          _.extend(arena, {
            player: drones[0],
            rival: drones[1]
          });
          arena.simulation = new Simulation(arena, socket);

          // Stop on disconnect
          socket.on("disconnect", arena.simulation.stop);
        });
    });
});
