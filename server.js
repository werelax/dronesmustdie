/* globals console */

"use strict";

var app = require("./config"),
    models = require("./models"),
    auth = require("./simpleauth");
    //socketManager = require("./socketmanager");


require("./auth")(auth);
var controllers = require("./controllers")(app, auth, models);
require("./routes")(app, auth, controllers);

app.listen(3001);
