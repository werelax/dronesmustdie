"use strict";

var MongoClient = require("mongodb").MongoClient,
    ObjectID = require("mongodb").ObjectID,
    Q = require("q"),
    _ = require("underscore");


var client = Q.ninvoke(MongoClient,
                       "connect",
                       "mongodb://127.0.0.1:27017/dmd");

client.fail(function(e) {
  console.log("ERROR conectando a Mongo: ", e);
});


/* Utils */

function op(colname) {
  var args = [].slice.call(arguments, 1),
      collections = {};
  return client.then(function(db) {
    if (!(colname in collections)) { collections[colname] = db.collection(colname); }
    return Q.ninvoke.apply(Q, [collections[colname]].concat(args));
  })
    .fail(function(err) {
      console.log("[MongoDB]", err);
      throw err;
    });
}

function makeOp(col) {
  var args = [].slice.call(arguments, 1);
  return function () { return col.apply({}, args); };
}

// Model utils

var utils = {
  findArray: function() {
    return this.apply(this, arguments)
      .then(function(result) {
        return Q.ninvoke(result, "toArray");
      });
  }
};

// Remotron approach

exports.users = _.extend(op.bind({}, "users"), utils, {
});

exports.drones = _.extend(op.bind({}, "drones"), utils, {

});

exports.matches = _.extend(op.bind({}, "matches"), utils, {
});

exports.op = op;
exports.makeOp = makeOp;
